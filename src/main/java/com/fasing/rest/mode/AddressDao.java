package com.fasing.rest.mode;

import com.fasing.rest.bean.Address;
import com.fasing.rest.mode.mapper.AddressMapper;
import org.apache.log4j.Logger;


import java.util.ArrayList;
import java.util.List;

public class AddressDao {

    private static final Logger logger = Logger.getLogger(AddressDao.class);

    public List<Address> queryAddressbyUid(int uid) {

        List<Address> addr = new ArrayList<Address>();
        try (DaoBase dao = new DaoBase()) {

            AddressMapper maper = dao.dynamicCast(AddressMapper.class);
            addr = maper.queryAddressbyUid(3);
        } catch (Exception e) {
            logger.error("queryAddressbyUid failed", e);
        }

        return addr;
    }

    public int insertAddress(Address a) {

        int n = 0;
        try (DaoBase dao = new DaoBase(true)) {

            AddressMapper maper = dao.dynamicCast(AddressMapper.class);
            n = maper.insertAddress(a);
        } catch (Exception e) {
            logger.error("insertAddress failed", e);
        }

        return n;
    }

    public int removeAddress(int uid) {
        int n = 0;
        try (DaoBase dao = new DaoBase(true)) {

            AddressMapper maper = dao.dynamicCast(AddressMapper.class);
            n = maper.removeAddress(uid);
        } catch (Exception e) {
            logger.error("insertAddress failed", e);
        }

        return n;
    }

    public int removeAddressByid(int id) {
        int n = 0;
        try (DaoBase dao = new DaoBase(true)) {

            AddressMapper maper = dao.dynamicCast(AddressMapper.class);
            n = maper.removeAddressByid(id);
        } catch (Exception e) {
            logger.error("insertAddress failed", e);
        }

        return n;
    }

}
