package com.fasing.rest.mode;

import com.fasing.rest.bean.GoodsSection;
import com.fasing.rest.mode.mapper.GoodsSectionMapper;
import org.apache.log4j.Logger;

import java.util.List;

public class GoodsSectionDao {

    private static final Logger logger = Logger.getLogger(GoodsSectionDao.class);

    public List<GoodsSection> querySection() {
        List<GoodsSection> g = null;
        try (DaoBase dao = new DaoBase()) {

            GoodsSectionMapper maper = dao.dynamicCast(GoodsSectionMapper.class);
            g = maper.querySection();
        } catch (Exception e) {
            logger.error("querySection failed", e);
        }
        return g;
    }

    public int updateSection(GoodsSection section) {
        int g = 0;
        try (DaoBase dao = new DaoBase(true)) {

            GoodsSectionMapper maper = dao.dynamicCast(GoodsSectionMapper.class);
            g = maper.updateSection(section);
        } catch (Exception e) {
            logger.error("updateSection failed", e);
        }
        return g;
    }

    public int removeSection(int id) {
        int g = 0;
        try (DaoBase dao = new DaoBase(true)) {

            GoodsSectionMapper maper = dao.dynamicCast(GoodsSectionMapper.class);
            g = maper.removeSection(id);
        } catch (Exception e) {
            logger.error("removeSection failed", e);
        }
        return g;
    }

    public int insertSection(GoodsSection section) {
        int g = 0;
        try (DaoBase dao = new DaoBase(true)) {

            GoodsSectionMapper maper = dao.dynamicCast(GoodsSectionMapper.class);
            g = maper.insertSection(section);
        } catch (Exception e) {
            logger.error("insertSection failed", e);
        }
        return g;
    }
}
