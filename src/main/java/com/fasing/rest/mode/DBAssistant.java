package com.fasing.rest.mode;

import org.apache.log4j.Logger;

public class DBAssistant {

    private static final Logger logger = Logger.getLogger(DBAssistant.class);
    private String config = "";

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
        try {
            MyBatisFactory.getInstance().init(this.config);
        } catch (Exception e) {
            logger.error("Mybatis intialize failed!!", e);
        }
    }
}
