package com.fasing.rest.mode;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;

public class MyBatisFactory {
    private static final Logger logger = Logger.getLogger(MyBatisFactory.class);
    private String config = "";

    SqlSessionFactory sqlSessionFactory = null;

    private static class single {
        private static MyBatisFactory instance = new MyBatisFactory();
    }

    public static MyBatisFactory getInstance() {
        return single.instance;
    }

    public boolean init(String xmlResource) {
        logger.info("mybatis config file is: " + xmlResource);
        InputStream inputStream = null;
        try {
            inputStream = MyBatisFactory.class.getClassLoader().getResourceAsStream(xmlResource);
        } catch (NullPointerException e) {
            logger.error("initialize mybatis failed!", e);
            return false;
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);

        return true;
    }

    public SqlSessionFactory getSqlSessionFactory() {
        return sqlSessionFactory;
    }

}
