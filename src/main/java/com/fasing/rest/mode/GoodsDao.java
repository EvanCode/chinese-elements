package com.fasing.rest.mode;

import com.fasing.rest.bean.Goods;
import com.fasing.rest.mode.mapper.GoodsMapper;
import org.apache.log4j.Logger;

import java.util.List;

public class GoodsDao {

    private static final Logger logger = Logger.getLogger(GoodsDao.class);

    public List<Goods> queryGoodsList(int min, int max) {
        List<Goods> g = null;
        try (DaoBase dao = new DaoBase()) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            g = maper.queryGoodsList(min, max);
        } catch (Exception e) {
            logger.error("queryGoodsList failed", e);
        }
        return g;
    }

    public Goods queryGoods(int id) {
        Goods g = null;
        try (DaoBase dao = new DaoBase()) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            g = maper.queryGoods(id);
        } catch (Exception e) {
            logger.error("queryGoods failed", e);
        }
        return g;

    }

    public int queryGoodsCount(String condition) {
        int g = 0;
        try (DaoBase dao = new DaoBase()) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            g = maper.queryGoodsCount(condition);
        } catch (Exception e) {
            logger.error("queryGoodsCount failed", e);
        }
        return g;
    }

    public int insetGoods(List<Goods> goodsList) {
        int g = 0;
        try (DaoBase dao = new DaoBase(true)) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            g = maper.insetGoods(goodsList);
        } catch (Exception e) {
            logger.error("insetGoods failed", e);
        }
        return g;
    }

    public int removeGoods(List<Integer> goodsIds) {
        int g = 0;
        try (DaoBase dao = new DaoBase(true)) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            g = maper.removeGoods(goodsIds);
        } catch (Exception e) {
            logger.error("removeGoods failed", e);
        }
        return g;

    }

    public int updateGoods(Goods g) {
        int r = 0;
        try (DaoBase dao = new DaoBase(true)) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            r = maper.updateGoods(g);
        } catch (Exception e) {
            logger.error("updateGoods failed", e);
        }
        return r;

    }

    public List<Goods> queryGoodsByCondition(String condition) {
        List<Goods> g = null;
        try (DaoBase dao = new DaoBase()) {

            GoodsMapper maper = dao.dynamicCast(GoodsMapper.class);
            g = maper.queryGoodsByCondition(condition);
        } catch (Exception e) {
            logger.error("queryGoodsByCondition failed", e);
        }
        return g;
    }

}
