package com.fasing.rest.mode;

import com.fasing.rest.bean.BannerInfo;
import com.fasing.rest.mode.mapper.BannerMapper;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class BannerDao {

    private static final Logger logger = Logger.getLogger(BannerDao.class);

    public List<BannerInfo> queryBanners() {
        List<BannerInfo> r = new ArrayList<>();
        try (DaoBase dao = new DaoBase()) {

            BannerMapper maper = dao.dynamicCast(BannerMapper.class);
            r = maper.queryBanners();
        } catch (Exception e) {
            logger.error("queryBanners failed", e);
        }

        return r;
    }

    public int updateBanner(BannerInfo info) {
        int r = 0;
        try (DaoBase dao = new DaoBase(true)) {

            BannerMapper maper = dao.dynamicCast(BannerMapper.class);
            r = maper.updateBanner(info);
        } catch (Exception e) {
            logger.error("updateBanner failed", e);
        }

        return r;

    }

    public int removeBanner(int id) {
        int r = 0;
        try (DaoBase dao = new DaoBase(true)) {

            BannerMapper maper = dao.dynamicCast(BannerMapper.class);
            r = maper.removeBanner(id);
        } catch (Exception e) {
            logger.error("removeBanner failed", e);
        }

        return r;

    }

    public int insertBanner(BannerInfo info) {
        int r = 0;
        try (DaoBase dao = new DaoBase(true)) {

            BannerMapper maper = dao.dynamicCast(BannerMapper.class);
            r = maper.insertBanner(info);
        } catch (Exception e) {
            logger.error("insertBanner failed", e);
        }

        return r;
    }
}
