package com.fasing.rest.mode;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

public class DaoBase implements AutoCloseable {

    private static final Logger logger = Logger.getLogger(DaoBase.class);
    private SqlSession session = null;

    public DaoBase() {
        session = MyBatisFactory.getInstance().getSqlSessionFactory().openSession();
        logger.info("mybatis session open!*");
    }

    public DaoBase(boolean open) {
        session = MyBatisFactory.getInstance().getSqlSessionFactory().openSession(open);
    }

    @Override
    public void close() {

        if (session != null) {
            try {
                session.close();
                logger.info("mybatis session closed!!!");
            } catch (Throwable e) {
                logger.error("mapper class:", e);
            }
        }
    }

    public <T> T dynamicCast(Class<T> tClass) {

        if (session != null) {
            logger.info("dynamic cast mapper!  [" + tClass.toString() + "]");
            return session.getMapper(tClass);
        }
        return null;
    }
}
