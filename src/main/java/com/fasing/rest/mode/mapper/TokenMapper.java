package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.Token;

public interface TokenMapper {

    public int insertToken(Token t);

    public Token queryToken(String tokenHash);

    public int removeToken(String tokenHash);
}
