package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.Order;

import java.util.List;

public interface OrderMapper {

    public int pushOrder(Order order);

    public int removeOder(int id);

    public List<Order> queryOrderByConditiion(String condition);

    public int updateOrder(Order order);
}
