package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.User;

public interface UserMapper {

    public int checkUserRegistry(String mail);

    public User queryUser(String mail);

    public int registerUser(User user);

    public int updatePasswd(String mail, String password);
}
