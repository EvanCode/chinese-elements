package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.BannerInfo;

import java.util.List;

public interface BannerMapper {
    public List<BannerInfo> queryBanners();

    public int updateBanner(BannerInfo info);

    public int removeBanner(int id);

    public int insertBanner(BannerInfo info);
}
