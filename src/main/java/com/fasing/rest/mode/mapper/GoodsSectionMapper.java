package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.GoodsSection;

import java.util.List;

public interface GoodsSectionMapper {

    public List<GoodsSection> querySection();

    public int updateSection(GoodsSection section);

    public int removeSection(int id);

    public int insertSection(GoodsSection section);
}
