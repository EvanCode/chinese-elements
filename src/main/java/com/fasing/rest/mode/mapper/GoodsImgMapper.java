package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.GoodsImg;

import java.util.List;

public interface GoodsImgMapper {


    public List<GoodsImg> queryGoodsImg(int goodid);

    public int updateGoodsImg(GoodsImg img);


    public int removeImgByGoodsId(int goodsId);


    public int removeImgByImgId(int imgId);

    public int addImgForGoods(GoodsImg img);
}
