package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.Goods;

import java.util.List;

public interface GoodsMapper {

    public List<Goods> queryGoodsList(int min, int max);

    public Goods queryGoods(int id);

    public int queryGoodsCount(String condition);

    public int insetGoods(List<Goods> goodsList);

    public int removeGoods(List<Integer> goodsIds);

    public int updateGoods(Goods g);

    public List<Goods> queryGoodsByCondition(String con);
}
