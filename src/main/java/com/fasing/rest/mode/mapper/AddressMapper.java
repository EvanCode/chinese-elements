package com.fasing.rest.mode.mapper;

import com.fasing.rest.bean.Address;

import java.util.List;

public interface AddressMapper {

    public List<Address> queryAddressbyUid(int uid);

    public int insertAddress(Address a);

    public int removeAddress(int uid);

    public int removeAddressByid(int id);
}
