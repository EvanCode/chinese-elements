package com.fasing.rest.mode;

import com.fasing.rest.bean.User;
import com.fasing.rest.mode.mapper.UserMapper;
import org.apache.log4j.Logger;

public class UserDao {

    private static final Logger logger = Logger.getLogger(UserDao.class);

    public boolean checkUserRegistry(String mail) {

        int check = 0;
        try (DaoBase dao = new DaoBase()) {

            UserMapper maper = dao.dynamicCast(UserMapper.class);
            check = maper.checkUserRegistry(mail);

            logger.info("check parameters:[" + mail + "], result is: " + check);
        } catch (Exception e) {
            logger.error("failed", e);
        }

        return check > 0;
    }

    public User queryUser(String mail) {

        User u = null;
        try (DaoBase dao = new DaoBase()) {

            UserMapper maper = dao.dynamicCast(UserMapper.class);
            u = maper.queryUser(mail);
            logger.info("query user info:[" + mail + "], result is: " + u);
        } catch (Exception e) {
            logger.error("failed", e);
        }

        return u;
    }

    public boolean rigestryUser(User u) {

        int check = 0;
        try (DaoBase dao = new DaoBase(true)) {

            UserMapper maper = dao.dynamicCast(UserMapper.class);
            check = maper.registerUser(u);

            logger.info("registry user:[" + u + "], result is: " + check);
        } catch (Exception e) {
            logger.error("err", e);
        }

        return check > 0;
    }

    public boolean updatePasswd(String mail, String password) {
        int check = 0;
        try (DaoBase dao = new DaoBase(true)) {

            UserMapper maper = dao.dynamicCast(UserMapper.class);
            check = maper.updatePasswd(mail, password);

            logger.info("update password:[" + mail + "], result is: " + check);
        } catch (Exception e) {
            logger.error("err", e);
        }

        return check > 0;
    }

}
