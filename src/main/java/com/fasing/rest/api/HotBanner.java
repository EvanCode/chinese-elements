package com.fasing.rest.api;

import com.fasing.rest.bean.*;
import com.fasing.rest.mode.*;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@RequestMapping("/api")
@RestController
public class HotBanner {

    private static final Logger logger = Logger.getLogger(HotBanner.class);

    @RequestMapping("/hotinfo")
    public List<BannerInfo> getBannerInfo() {

        List<BannerInfo> infos = new ArrayList<>();
        BannerDao dao = new BannerDao();
        List<BannerInfo> r = dao.queryBanners();
        if (r == null || r.isEmpty()) {
            logger.error("can not find banner info in database!!!!");
        } else {
            infos = r;
        }

        return infos;
    }

    @RequestMapping(value = "/hotgoodssection", method = RequestMethod.POST)
    public List<GoodsSection> getHotGoodsSections(@RequestBody GoodsSectionRequest rq){
        List<GoodsSection> g = new ArrayList<>();
        GoodsSectionDao dao = new GoodsSectionDao();
        List<GoodsSection> sections = dao.querySection();
        if (sections == null || sections.isEmpty()) {
            logger.error("can not find section in database!!!!!");
            return g;
        }

        GoodsDao gDao = new GoodsDao();
        for (GoodsSection a : sections) {
            int id = a.getId();
            if (id == 3) id = 1;
            String con = "type=%d limit 4";
            List<Goods> glist = gDao.queryGoodsByCondition(String.format(con, id));
            logger.info("goods result: " + ((glist == null) ? null : glist.size()));
            if (glist != null) {
                a.setProducts(glist);
            }
        }
        if (sections != null) {
            g = sections;
        }

        return g;
    }

    @RequestMapping("/test")
    public String test() {


        try {
            GoodsDao dao = new GoodsDao();
            List<Goods> t = dao.queryGoodsByCondition("type=1 limit 4");
            System.out.println(t.size());
            logger.info("t size:" + t.size());

        } catch (Exception e) {
            logger.error("xxhh", e);
        }

        return "OK!!!!";
    }


}
