package com.fasing.rest.auth;


import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import org.apache.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class UserTokenInterceptorImpl extends UserTokenInterceptor {

    final static String X_AUTH_TOKEN = "x_auth_token";

    private static final Logger logger = Logger.getLogger(UserTokenInterceptorImpl.class);


    @Override
    public boolean preHandle(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object handler) throws Exception {
        String token = request.getParameter(X_AUTH_TOKEN);
        logger.info(request.getRequestURI());
        if (token != null) {
            JwtToken tokenUtil = new JwtToken();
            Claims user = tokenUtil.unsign(token);
            logger.info(user);

            return true;
        } else {
            return true;
        }

       /* Object responseData = ResponseEntity.noContent();
        response.setContentType("application/json; charset=utf-8");
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(responseData);
        PrintWriter out = response.getWriter();
        out.print(json);
        out.flush();
        out.close();*/

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
    }
}
