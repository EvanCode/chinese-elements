package com.fasing.rest.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

public class JwtToken {


    public static final String JWT_SECRET = "RXZhbjI2OThAZ21haWwuY29t";
    public static final int JWT_REFRESH_INTERVAL = 55 * 60 * 1000;  //millisecond

    public SecretKey generalKey() {
        String stringKey = JWT_SECRET;
        SecretKey key = new SecretKeySpec(stringKey.getBytes(), 0, stringKey.getBytes().length, "AES");
        return key;
    }

    public String sign(String id, String usr, String role) throws UnsupportedEncodingException, JsonProcessingException {

        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        long expMillis = nowMillis + JWT_REFRESH_INTERVAL;
        Date exp = new Date(expMillis);
        SecretKey key = generalKey();
        JwtBuilder builder = Jwts.builder()
                .setId(id)
                .setIssuedAt(now)
                .setSubject(usr)
                .signWith(signatureAlgorithm, key)
                .setAudience(role)
                .setExpiration(exp);
        return builder.compact();
    }


    public Claims unsign(String token) throws IOException {

        SecretKey key = generalKey();
        Claims claims = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token).getBody();
        claims.getExpiration();
        return claims;
    }

}
