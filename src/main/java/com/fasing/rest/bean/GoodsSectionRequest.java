package com.fasing.rest.bean;

public class GoodsSectionRequest {

    private String sectiontype = "";

    public String getSectiontype() {
        return sectiontype;
    }

    public void setSectiontype(String sectiontype) {
        this.sectiontype = sectiontype;
    }
}
