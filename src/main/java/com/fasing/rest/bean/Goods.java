package com.fasing.rest.bean;

public class Goods
{
    private String productname = "";
    private  int rate = 0;
    private double preprice = 0.0;
    private double price = 0.0;
    private double sale = 0.0;
    private String productthumb = "";
    private String productlabel = "";
    private int id = -1;
    private int type = -1;
    private int availability = 0;
    private String detail = "";
    private int width = 0;
    private int height = 0;
    private int length = 0;
    private String author = "";

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public double getPreprice() {
        return preprice;
    }

    public void setPreprice(double preprice) {
        this.preprice = preprice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSale() {
        return sale;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    public String getProductthumb() {
        return productthumb;
    }

    public void setProductthumb(String productthumb) {
        this.productthumb = productthumb;
    }

    public String getProductlabel() {
        return productlabel;
    }

    public void setProductlabel(String productlabel) {
        this.productlabel = productlabel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
