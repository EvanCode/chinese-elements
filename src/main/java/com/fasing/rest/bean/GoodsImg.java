package com.fasing.rest.bean;

public class GoodsImg {

    private int id = -1;
    private int goodsId = -1;
    private String detailthumb = "";
    private String detailImg = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public String getDetailImg() {
        return detailImg;
    }

    public void setDetailImg(String detailImg) {
        this.detailImg = detailImg;
    }

    public String getDetailthumb() {
        return detailthumb;
    }

    public void setDetailthumb(String detailthumb) {
        this.detailthumb = detailthumb;
    }
}
