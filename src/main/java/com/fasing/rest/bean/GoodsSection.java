package com.fasing.rest.bean;

import java.util.ArrayList;
import java.util.List;

public class GoodsSection {

    private String title = "";
    private String bannerimg = "";
    private String bannerdes = "";
    private String intr = "";
    private int firstGoods = -1;
    private List<Goods> products = new ArrayList<Goods>();
    private int id = 0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBannerimg() {
        return bannerimg;
    }

    public void setBannerimg(String bannerimg) {
        this.bannerimg = bannerimg;
    }

    public String getBannerdes() {
        return bannerdes;
    }

    public void setBannerdes(String bannerdes) {
        this.bannerdes = bannerdes;
    }


    public List<Goods> getProducts() {
        return products;
    }

    public void setProducts(List<Goods> products) {
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIntr() {
        return intr;
    }

    public void setIntr(String intr) {
        this.intr = intr;
    }
}
