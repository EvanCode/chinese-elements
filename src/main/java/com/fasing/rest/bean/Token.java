package com.fasing.rest.bean;

public class Token {
    private String token = "";
    private int uid = -1;
    private String tokenHash = "";

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }
}
