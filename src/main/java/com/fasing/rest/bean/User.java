package com.fasing.rest.bean;

import java.util.Date;

public class User {
    private String nickName = "";
    private String mail = "";
    private String passwd = "";
    private Date created = null;
    private int id = -1;


    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toString() {
        return "nickNanme: " + this.nickName + "\n"
                + "id: " + this.id + "\n"
                + "mail: " + this.mail + "\n"
                + "passwd: ***********\n"
                + "created: " + this.getCreated().toString() + "\n";
    }
}
