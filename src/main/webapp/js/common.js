var COMMONNAMESPACE = window.NameSpace || {};
COMMONNAMESPACE.namespace = new function () {
    var self = this;


    self.guid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    self.createProductLine = function ($, singleJson) {

        var singleProduct = $("<div></div>");
        singleProduct.addClass("product product-single");

        var thumb = $("<div></div>");
        thumb.addClass("product-thumb");
        thumb.appendTo(singleProduct);

        if (singleJson["productlabel"] != "") {
            var label = $("<div></div>");
            label.addClass("product-label");
            label.appendTo(thumb);

            var contentText = $("<span></span>");
            contentText.html(singleJson["productlabel"]);
            contentText.appendTo(label);

            var con = parseInt(singleJson["sale"] * 100);
            if (con != 0) {
                var sale = $("<span></span>");
                sale.addClass("sale");
                sale.html("-" + con + "%");
                sale.appendTo(label);
            }
        }

        var thumbImg = $("<img></img>");
        thumbImg.attr("src", singleJson["productthumb"]);
        thumbImg.appendTo(thumb);


        var productbottom = $("<div></div>");
        productbottom.addClass("product-body");
        productbottom.appendTo(singleProduct);

        var h3 = $("<h3></h3>");
        h3.addClass("h3");
        h3.html("$" + singleJson["price"].toFixed(2));
        h3.appendTo(productbottom);


        var old = parseInt(singleJson["sale"] * 100);
        if (old != 0) {

            var oldp = singleJson["price"] / (1 - singleJson["sale"]);
            var rprice = $("<del></del>");
            rprice.addClass("product-old-price");
            rprice.html("$" + oldp.toFixed(2));
            rprice.appendTo(h3);
        }


        var rateFrame = $("<div></div>");
        rateFrame.addClass("product-rating");
        rateFrame.appendTo(productbottom);

        var rate = singleJson["rate"];
        for (var i = 0; i < rate; ++i) {
            var star = $("<i></i>");
            star.addClass("fa fa-star");
            star.appendTo(rateFrame);
        }

        var rest = 5 - rate;
        if (rest < 0) {
            rest = 0;
        }
        for (var j = 0; j < rest; ++j) {
            var star = $("<i></i>");
            star.addClass("fa fa-star-o empty");
            star.appendTo(rateFrame);
        }

        var productName = $("<h2></h2>");
        productName.addClass("product-name");
        productName.appendTo(productbottom);

        var nameText = $("<a></a>");
        nameText.attr("href", "#");
        nameText.html(singleJson["productname"]);
        nameText.appendTo(productName);

        var productsBtn = $("<div></div>");
        productsBtn.addClass("product-btns");
        productsBtn.appendTo(productbottom);

        var heart = $("<button></button>");

        var s = self.getHeart(singleJson["id"]);
        var sc = "icon-btn main-btn";

        if (true == s) {
            sc = "main-btn-click";
        }
        heart.addClass(sc);
        heart.html("<i class=\"fa fa-heart\"></i>");
        heart.click(function () {
            var bv = self.getHeart(singleJson["id"]);
            console.log("value is: " + bv);
            self.saveHeart(singleJson["id"], !bv);
            var sc = "icon-btn main-btn";
            if (bv) {
                sc = "main-btn-click";
            }
            console.log("ok~~~~~~" + sc);
            heart.removeClass();
            heart.addClass(sc);
            heart.blur();
        });


        heart.appendTo(productsBtn);


        var exchange = $("<button></button>");
        exchange.addClass("main-btn icon-btn");
        exchange.html("<i class=\"fa fa-exchange\"></i>");
        exchange.appendTo(productsBtn);

        var id = self.guid();
        var cert = $("<button></button>");
        cert.attr("id", id + "");
        cert.addClass("primary-btn add-to-cart");
        cert.html("<i class=\"fa fa-shopping-cart\"></i> Add to Cart");
        cert.appendTo(productsBtn);

        cert.click(function () {
            var item = singleJson;
            var certjsonText = self.getLocalData("cert-data");
            if (certjsonText == undefined) {
                var certjsonText = "{\"data\" :[]}";
            }
            certjson = JSON.parse(certjsonText);

            var cert = certjson["data"];
            cert[cert.length] = item;
            self.saveLocalData("cert-data", JSON.stringify(certjson));
            self.renderCert($, certjson);

            $('#' + id).grumble(
                {
                    text: 'Yes, my master!',
                    angle: 180,
                    distance: 0,
                    showAfter: 200,
                    type: 'alt-',
                    hideAfter: 200
                }
            );

        });

        return singleProduct;

    };


    self.slickAction = function ($) {

        // PRODUCT DETAILS SLICK
        $('#product-main-view').slick({
            infinite: true,
            speed: 300,
            dots: false,
            arrows: true,
            fade: true,
            asNavFor: '#product-view',
        });

        $('#product-view').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            centerMode: true,
            focusOnSelect: true,
            asNavFor: '#product-main-view',
        });

        // PRODUCT ZOOM
        $('#product-main-view .product-view').zoom();


        var slider = document.getElementById('price-slider');
        if (slider) {
            noUiSlider.create(slider, {
                start: [1, 999],
                connect: true,
                tooltips: [true, true],
                format: {
                    to: function (value) {
                        return value.toFixed(2) + '$';
                    },
                    from: function (value) {
                        return value
                    }
                },
                range: {
                    'min': 1,
                    'max': 999
                }
            });
        }

    };

    self.saveSessionData = function (key, data) {
        sessionStorage.setItem(key, data);
    };

    self.getSessionData = function (key) {
        var v = sessionStorage.getItem(key);
        return v;
    }

    self.saveLocalData = function (key, data) {
        localStorage.setItem(key, data);
    };
    self.getLocalData = function (key) {
        var value = localStorage.getItem(key);
        return value;
    };

    self.saveHeart = function (goodsid, value) {
        var key = "heart-" + goodsid;
        console.log("save value[" + key + "] value:" + value);
        self.saveLocalData(key, value);
    };

    self.getHeart = function (goodsid) {
        var key = "heart-" + goodsid;
        var v = self.getLocalData(key);
        console.log("read value:[" + key + "] " + v);
        if (v == "true") {
            return true;
        }
        return false;
    };


    self.createOneCart = function ($, product) {
        var widget = $("<div></div>");
        widget.addClass("product product-widget");

        var thumb = $("<div></div>");
        thumb.addClass("product-thumb");

        var img = $("<img></img>");
        img.attr("src", product["productthumb"]);
        thumb.append(img);
        widget.append(thumb);

        var pbody = $("<div></div>");
        pbody.addClass("product-body");

        var h3 = $("<h4></h4>");
        h3.html("$" + product["price"].toFixed(2));
        h3.appendTo(pbody);

        var pname = $("<h4></h4>");
        pname.html("<a href=\"#\">" + product["productname"] + "</a>");
        pname.appendTo(pbody);

        pbody.appendTo(widget);

        var btnID = self.guid();

        var cancel = $("<button></button>");
        cancel.attr("id", btnID);
        cancel.addClass("cancel-btn");
        var trash = $("<i></i>");
        trash.addClass("fa fa-trash");
        cancel.append(trash);
        cancel.appendTo(widget);
        cancel.click(function (e) {
            alert("not implements!!");

        });



        return widget;
    };

    self.loadCert = function ($) {
        $("#id-count-cart").html("0");
        $("#money-pass").html("0$");
        $("#shopping-cart-list-content").empty();

        var certjson = self.getLocalData("cert-data");
        if (certjson == undefined) {
            return;
        }

        var cert = JSON.parse(certjson);
        if (cert == undefined) {
            return;
        }

        self.renderCert($, cert);
    }


    self.renderCert = function ($, cert) {
        $("#id-count-cart").html("0");
        $("#money-pass").html("0$");
        $("#shopping-cart-list-content").empty();

        var totalCount = 0;
        var list = cert["data"];
        for (var i = 0; i < list.length; ++i) {
            var item = list[i];
            $("#shopping-cart-list-content").append(self.createOneCart($, item));
            totalCount = totalCount + item["price"];
        }

        $("#id-count-cart").html(list.length + "");
        $("#money-pass").html(totalCount.toFixed(2) + "$");
    };

}