var CENameSpace = window.NameSpace || {};
CENameSpace.PreLoad = new function() {
    var self = this;

    //load main page content
    self.preLoadMain = function($) {
        self.fetchHotContent($);
        self.fetchProductData($,"sss",0);

        $(document).ready(function () {
            COMMONNAMESPACE.namespace.loadCert($);
        });
    };

    //load hot art
    self.fetchHotContent = function($){

        $.getJSON("/rest/api/hotinfo", function (result, status) {
            if ("success" === status) {
                if (result != undefined) {
                    self.parseBanner($, result);
                }
            }
        });
    };
    

    //parse the banner
    self.parseBanner = function($, json) {

        if (json.length > 0) {
            $("#home-slick").empty();
        }

        for (var i = 0; i < json.length; i++) {

            var img = document.createElement("img");
            img.setAttribute("class", ".banner_img");
            img.setAttribute("id", "bander-item-" + (i + 1));
            console.log(json[i]["imgSrc"]+"");
            img.setAttribute("src", json[i]["imgSrc"]);
            img.setAttribute("alt", "");
            img.setAttribute("style", " width: 100%;" +
                "  max-height:480px;" +
                "  max-width: 1200px;");

            var banner = document.createElement("div");
            banner.setAttribute("class","banner banner-1");
            banner.appendChild(img);

            $("#home-slick").append(banner);
        }

        // HOME SLICK
        $('#home-slick').slick({
            autoplay: true,
            infinite: true,
            speed: 300,
            arrows: true,
        });
    };



    //
    self.fetchProductData = function ($, productType, numbers) {

        // build request

        var jsondata = '{\n' +
            '"sectiontype":"all"\n' +
            '}';
        $.ajax({
            url: '/rest/api/hotgoodssection',
            headers: {
                'Content-Type': 'application/json'
            },
            data: jsondata,
            dataType: 'JSON',
            async: true,
            type: 'POST',
            success: function (json) {
                self.activeProductsSection($,json);
            },
            error: function () {
                alert("server can not handle this request!!!");
            }
        });
    };

    self.activeProductsSection = function($, json){

        if (json.length > 0) {
            $(".section").first().remove();
            $(".section").first().remove();
            $(".section").first().remove();

            var prefixProducts = "product-slick-";
            for (var i = 0; i < json.length; ++i) {
                var index = json.length - 1 - i;

                var data = json[index];
                var s0 = self.createProductSection($, data, prefixProducts + index);
                $("#home").after(s0);
            }

            for (var j = 0; j < json.length; ++j) {
                self.actionSlick($, prefixProducts + j, j + 1 + "");
            }

            COMMONNAMESPACE.namespace.slickAction($);
        }
        else {
            alert("server error!");
        }
    }


    //create product section
    self.createProductSection = function ($, json, slickid) {
        var sectiondiv = $('<div></div>');
        sectiondiv.addClass("section");

        var containerdiv = $('<div></div>');
        containerdiv.addClass("container");
        containerdiv.appendTo(sectiondiv);

        var rowdiv = $('<div></div>');
        rowdiv.addClass("row");
        rowdiv.appendTo(containerdiv);

        self.createSectionTitle($, json["title"]).appendTo(rowdiv);
        self.createSectionBanner($,json["bannerimg"]+"",json["bannerdes"]+"",json["intr"]).appendTo(rowdiv);
        self.createProductSlick($, json, slickid).appendTo(rowdiv);
        return sectiondiv;
    };

    self.actionSlick = function ($, sectionid, id) {
        $('#'+ sectionid).slick({
            slidesToShow: 3,
            slidesToScroll: 2,
            autoplay: true,
            infinite: true,
            speed: 300,
            dots: true,
            arrows: false,
            appendDots: '.product-slick-dots-' + id,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
                {
                    breakpoint: 480,
                    settings: {
                        dots: false,
                        arrows: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
            ]
        })

    };




    self.createProductSlick = function($, section, sectionid){

        var slick = $("<div></div>");
        slick.addClass("col-md-9 col-sm-6 col-xs-6");

        var row = $("<div></div>");
        row.addClass("row");
        row.appendTo(slick);

        var slickframe = $("<div></div>");
        slickframe.addClass("product-slick");
        slickframe.attr("id", sectionid);
        slickframe.appendTo(row);

        if (section["products"].length > 0) {
            for (var i = 0; i < section["products"].length; ++i){
                COMMONNAMESPACE.namespace.createProductLine($, section["products"][i]).appendTo(slickframe);
            }
        }
        return slick;
    };



    self.createSectionTitle = function ($, contentName) {

        var container = $("<div></div>");
        container.addClass("col-md-12");

        var sectionTitle=$("<div></div>");
        sectionTitle.addClass("section-title");
        sectionTitle.appendTo(container);

        var title = $('<h2>');
        title.addClass("title");
        title.html(contentName);
        title.appendTo(sectionTitle);

        var pullright = $("<div></div>");
        pullright.addClass("pull-right");
        pullright.appendTo(sectionTitle);

        var slickdot = $("<div></div>");
        slickdot.addClass("product-slick-dots-1 custom-dots");
        slickdot.appendTo(sectionTitle);

        return container;
    };

    self.createSectionBanner = function ($, bimg, btnText, instroduce) {

        var bannerContainer = $("<div></div>");
        bannerContainer.addClass("col-md-3 col-sm-6 col-xs-6");

        var banner = $("<div></div>");
        banner.addClass("banner banner-2");
        banner.appendTo(bannerContainer);

        var bannerImg = $("<img></img>");

        var imgpath = bimg + "";
        bannerImg.addClass("banner2img")
        console.log(banner);
        bannerImg.attr("src", imgpath);
        bannerImg.appendTo(banner);

        var caption = $("<div></div>");
        caption.addClass("banner-caption");
        caption.appendTo(banner);

        var h2Text = $("<h2></h2>");
        h2Text.addClass("white-color");
        h2Text.html(instroduce);
        h2Text.appendTo(caption);

        if (btnText != "") {
            var btn = $("<button></button>");
            btn.addClass("primary-btn");
            btn.html(btnText);
            btn.appendTo(caption);
        }

        return bannerContainer;
    };
};

CENameSpace.PreLoad.preLoadMain(jQuery);