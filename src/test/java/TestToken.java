import com.fasing.rest.auth.JwtToken;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.Claims;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class TestToken {

    @Test
    public void testTokenFunc0() throws IOException {

        JwtToken token = new JwtToken();
        String parse = token.sign("1000", "Evan", "zhang");

        System.out.println(parse);

        Claims r = token.unsign(parse);
        System.out.println(r);

    }

}
