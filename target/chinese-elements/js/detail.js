var DETAILNAMESPACE = window.NameSpace || {};
DETAILNAMESPACE.LOAD_CONTENT = new function() {

    var self = this;

    self.pre_Load = function ($) {

        self.query_goodsDetail($,"sss");
        COMMONNAMESPACE.namespace.slickAction($);
    };

    self.query_goodsDetail = function($, goodid){


        var mockText = "{\n" +
            "\t\"productname\": \"Product Name Goes Here\",\n" +
            "\t\"rate\": 4,\n" +
            "\t\"preprice\": 45.00,\n" +
            "\t\"price\": 32.50,\n" +
            "\t\"sale\": \"-20%\",\n" +
            "\t\"productthumb\": \"./img/product07.jpg\",\n" +
            "\t\"productlabel\": \"New\",\n" +
            "\t\"availability\": 1,\n" +
            "\t\"detailImg\": [\n" +
            "\t\t\"./img/main-product01.jpg\",\n" +
            "\t\t\"./img/main-product02.jpg\",\n" +
            "\t\t\"./img/main-product03.jpg\",\n" +
            "\t\t\"./img/main-product04.jpg\"\n" +
            "\t],\n" +
            "\t\"thumbImg\": [\n" +
            "\t\t\"./img/thumb-product01.jpg\",\n" +
            "\t\t\"./img/thumb-product02.jpg\",\n" +
            "\t\t\"./img/thumb-product03.jpg\",\n" +
            "\t\t\"./img/thumb-product04.jpg\"\n" +
            "\t],\n" +
            "\t\"detail\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\n" +
            "}";

        var detailJson = JSON.parse(mockText);

        $("#product-main-view").empty();
        var imgList = detailJson["detailImg"];
        for (var i = 0; i < imgList.length; ++i){
            $("#product-main-view").append(self.createPreview($,"product-view", imgList[i]));
        }

        $("#product-view").empty();
        var thumbImg = detailJson["thumbImg"];
        for (var i = 0; i < thumbImg.length; ++i){
            $("#product-view").append(self.createPreview($,"product-view", thumbImg[i]));
        }

        self.renderDetail($,detailJson);

        var pickframe = $("<div></div>");
        pickframe.addClass("col-md-3 col-sm-6 col-xs-6")
        COMMONNAMESPACE.namespace.createProductLine($, detailJson).appendTo(pickframe);
        $("#pickforu-content").append(pickframe);

        var pickframe1 = $("<div></div>");
        pickframe1.addClass("col-md-3 col-sm-6 col-xs-6")
        COMMONNAMESPACE.namespace.createProductLine($, detailJson).appendTo(pickframe1);
        $("#pickforu-content").append(pickframe1);

        var pickframe2 = $("<div></div>");
        pickframe2.addClass("col-md-3 col-sm-6 col-xs-6")
        COMMONNAMESPACE.namespace.createProductLine($, detailJson).appendTo(pickframe2);
        $("#pickforu-content").append(pickframe2);

        var pickframe3 = $("<div></div>");
        pickframe3.addClass("col-md-3 col-sm-6 col-xs-6")
        COMMONNAMESPACE.namespace.createProductLine($, detailJson).appendTo(pickframe3);
        $("#pickforu-content").append(pickframe3);

    };

    self.createPreview = function($, cName, imgsrc){
        var prediv = $('<div></div>');
        prediv.addClass(cName);

        var preImg = $("<img></img>");
        preImg.attr("src", imgsrc);
        preImg.attr("alt", "");
        preImg.appendTo(prediv);

        return prediv;
    };

    self.renderDetail = function ($, goodsJson) {

        var parent = $("#product-detail-id");
        parent.empty();

        if (goodsJson["productlabel"] != "") {
            var label = $("<div></div>");
            label.addClass("product-label");

            var contentText =  $("<span></span>");
            contentText.html(goodsJson["productlabel"]);
            contentText.appendTo(label);

            var sale =  $("<span></span>");
            sale.addClass("sale");
            sale.html(goodsJson["sale"]);
            sale.appendTo(label);

            parent.append(label);
        }

        var productName =  $("<h2></h2>");
        productName.addClass("product-name");
        parent.append(productName);

        var nameText = $("<a></a>");
        nameText.attr("href", "#");
        nameText.html(goodsJson["productname"]);
        nameText.appendTo(productName);


        var h3 = $("<h3></h3>");
        h3.addClass("h3");
        h3.html("$" + goodsJson["price"].toFixed(2));

        var rprice = $("<del></del>");
        rprice.addClass("product-old-price");
        rprice.html("$" + goodsJson["preprice"].toFixed(2));
        rprice.appendTo(h3);
        parent.append(h3);



        var f = $("<div></div>");
        var rateFrame =  $("<div></div>");
        rateFrame.addClass("product-rating");
        rateFrame.appendTo(f);

        var rate = goodsJson["rate"];
        for (var i = 0; i < rate; ++i) {
            var star = $("<i></i>");
            star.addClass("fa fa-star");
            star.appendTo(rateFrame);
        }

        var rest = 5 - rate;
        if (rest < 0) {
            rest = 0;
        }
        for (var j =0; j < rest; ++j){
            var star = $("<i></i>");
            star.addClass("fa fa-star-o empty");
            star.appendTo(rateFrame);
        }
        f.appendTo(parent);

        var shelf = "In Stock";
        if (0 == goodsJson["availability"]){
            shelf = "Out of Stock";
        }
        var stock = $("<p></p>");
        stock.html("<strong>Availability:</strong> " + shelf);
        stock.appendTo(parent);

        var detail = $("<p></p>");
        detail.html(goodsJson["detail"]);
        detail.appendTo(parent);


        var popt = $("<div></div>");
        popt.addClass("product-options");
        popt.appendTo(parent);

        var sizeopt = $("<ul></ul>");
        sizeopt.addClass("size-option");
        sizeopt.appendTo(popt);

        var sizeText = $("<li></li>");
        sizeText.html("<span class=\"text-uppercase\">Size:</span>");
        sizeText.appendTo(sizeopt);

        var sizeValue = $("<li></li>");
        sizeValue.html("45*45(cm)");
        sizeValue.appendTo(sizeopt);

        var btn = $("<div></div>");
        btn.addClass("product-btns");
        btn.appendTo(parent);

        var inputbtn = $("<div></div>");
        inputbtn.addClass("qty-input");
        inputbtn.appendTo(btn);
        inputbtn.html("<span class=\"text-uppercase\">QTY: </span>\n" +
            "\t\t\t\t\t\t\t\t\t<input class=\"input\" type=\"number\">");

        var addBtn = $("<button></button>");
        addBtn.addClass("primary-btn add-to-cart");
        addBtn.appendTo(btn);
        addBtn.html("<i class=\"fa fa-shopping-cart\"></i> Add to Cart");

        var pushright =$("<div></div>");
        pushright.addClass("pull-right");
        pushright.appendTo(btn);
        pushright.html("<button class=\"main-btn icon-btn\"><i class=\"fa fa-heart\"></i></button>\n" +
            "\t\t\t\t\t\t\t\t\t<button class=\"main-btn icon-btn\"><i class=\"fa fa-exchange\"></i></button>\n" +
            "\t\t\t\t\t\t\t\t\t<button class=\"main-btn icon-btn\"><i class=\"fa fa-share-alt\"></i></button>");


        $("#tab1").html(goodsJson["detail"]);

    };

}

DETAILNAMESPACE.LOAD_CONTENT.pre_Load(jQuery);

