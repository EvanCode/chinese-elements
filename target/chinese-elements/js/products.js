var PRODUCT_JS_NAMESPACE = window.NameSpace || {};
PRODUCT_JS_NAMESPACE.init_load = new function () {

    var self = this;

    self.pre_Load = function ($) {

        var mockText = "{\n" +
            "\t\"productname\": \"Product Name Goes Here\",\n" +
            "\t\"rate\": 4,\n" +
            "\t\"preprice\": 45.00,\n" +
            "\t\"price\": 32.50,\n" +
            "\t\"sale\": \"-20%\",\n" +
            "\t\"productthumb\": \"./img/product07.jpg\",\n" +
            "\t\"productlabel\": \"New\",\n" +
            "\t\"availability\": 1,\n" +
            "\t\"detailImg\": [\n" +
            "\t\t\"./img/main-product01.jpg\",\n" +
            "\t\t\"./img/main-product02.jpg\",\n" +
            "\t\t\"./img/main-product03.jpg\",\n" +
            "\t\t\"./img/main-product04.jpg\"\n" +
            "\t],\n" +
            "\t\"thumbImg\": [\n" +
            "\t\t\"./img/thumb-product01.jpg\",\n" +
            "\t\t\"./img/thumb-product02.jpg\",\n" +
            "\t\t\"./img/thumb-product03.jpg\",\n" +
            "\t\t\"./img/thumb-product04.jpg\"\n" +
            "\t],\n" +
            "\t\"detail\": \"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"\n" +
            "}";

        var detailJson = JSON.parse(mockText);

        var store = $("#store_content");
        store.empty();

        for (var i = 0; i < 9; ++i) {

            var x6 = self.createFrame($);
            COMMONNAMESPACE.namespace.createProductLine($, detailJson).appendTo(x6);
            store.append(x6);

            if (i === 1 || i === 3 || i === 7) {
                var visiblexs = $("<div></div>");
                visiblexs.addClass("clearfix visible-sm visible-xs");
                store.append(visiblexs);
            }

            if (i === 2) {
                var visiblelg = $("<div></div>");
                visiblelg.addClass("clearfix visible-md visible-lg");
                store.append(visiblelg);
            }

            if (i === 5) {
                var visiblelg = $("<div></div>");
                visiblelg.addClass("clearfix visible-md visible-lg visible-sm visible-xs");
                store.append(visiblelg);
            }
        }
    };

    self.createFrame = function ($) {
        var x6 = $("<div></div>");
        x6.addClass("col-md-4 col-sm-6 col-xs-6");
        return x6;
    };
};

PRODUCT_JS_NAMESPACE.init_load.pre_Load(jQuery);