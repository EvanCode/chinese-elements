DROP DATABASE IF EXISTS `cedb`;
CREATE DATABASE `cedb`;
USE `cedb`;
DROP TABLE IF EXISTS `tuser`;
CREATE TABLE IF NOT EXISTS `tuser` (
  `user_id`       INT(11) UNSIGNED      AUTO_INCREMENT,
  `user_email`    VARCHAR(100) NOT NULL,
  `user_passwd`   VARCHAR(128) NOT NULL,
  `user_nickname` VARCHAR(128) NOT NULL,
  `create_date`   DATETIME     NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`user_id`)
)
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `tuaddress`;
CREATE TABLE IF NOT EXISTS `tuaddress` (
  `addr_id`      INT UNSIGNED AUTO_INCREMENT,
  `user_id`      INT,
  `addr_line`    VARCHAR(256) NOT NULL,
  `addr_city`    VARCHAR(50)  NOT NULL,
  `addr_state`   VARCHAR(50)  NOT NULL,
  `addr_zip`     VARCHAR(13)  NOT NULL,
  `addr_country` VARCHAR(64)  NOT NULL,
  `addr_phone`   VARCHAR(30)  NOT NULL,
  PRIMARY KEY (`addr_id`, `user_id`)
)
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `tgoods`;
CREATE TABLE IF NOT EXISTS `tgoods` (
  `id`           INT(11) UNSIGNED AUTO_INCREMENT,
  `pname`        VARCHAR(128)  NOT NULL,
  `rate`         INT              DEFAULT 0,
  `price`        FLOAT(8, 2),
  `sale`         FLOAT(8, 2),
  `label`        VARCHAR(64)   NOT NULL,
  `availability` INT              DEFAULT 0,
  `thumb`        VARCHAR(256)  NOT NULL,
  `detail`       VARCHAR(1024) NOT NULL,
  `type`         INT              DEFAULT -1,
  `width`        INT              DEFAULT 0,
  `height`       INT              DEFAULT 0,
  `length`       INT              DEFAULT 0,
  `author`       VARCHAR(64),
  PRIMARY KEY (id)
)
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `tgoodsimg`;
CREATE TABLE IF NOT EXISTS `tgoodsimg` (
  `id`          INT(11) UNSIGNED AUTO_INCREMENT,
  `goods_id`    INT(11)      NOT NULL,
  `detailthumb` VARCHAR(256) NOT NULL,
  `detailimg`   VARCHAR(256) NOT NULL,
  PRIMARY KEY (id, goods_id)
)
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `torder`;
CREATE TABLE IF NOT EXISTS `torder` (
  `id`       INT(16) UNSIGNED AUTO_INCREMENT,
  `goods_id` INT(11)  NOT NULL,
  `uid`      INT(11),
  `price`    FLOAT(8, 2),
  `state`    INT,
  `count`    INT              DEFAULT 1,
  `time`     DATETIME NOT NULL,
  PRIMARY KEY (id, goods_id)
)
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `ttoken`;
CREATE TABLE IF NOT EXISTS `ttoken` (
  `id`     INT AUTO_INCREMENT,
  `token`  VARCHAR(512) NOT NULL,
  `tokenh` VARCHAR(64)  NOT NULL,
  PRIMARY KEY (id, tokenh)
)
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `tbanner`;
CREATE TABLE IF NOT EXISTS `tbanner` (
  `id`       INT AUTO_INCREMENT,
  `goods_id` INT(11)      NOT NULL,
  `img`      VARCHAR(256) NOT NULL,
  `type`     INT,
  PRIMARY KEY (id)
)
  DEFAULT CHARSET = utf8;


DROP TABLE IF EXISTS `tgoodssection`;
CREATE TABLE IF NOT EXISTS `tgoodssection` (
  `id`         INT,
  `banner_des` VARCHAR(128) NOT NULL,
  `banner_img` VARCHAR(128) NOT NULL,
  `title`      VARCHAR(128) NOT NULL,
  `des`        VARCHAR(128) NOT NULL,
  `goods_id`   INT(11),
  PRIMARY KEY (id)
)
  DEFAULT CHARSET = utf8;
