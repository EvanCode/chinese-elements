USE `cedb`;
INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Fire',
  4,
  52.0,
  0.2,
  'New',
  1,
  './img/painting/thumb/01.jpg',
  'Strong local flavor and unique artistic style',
  1,
  39,
  0,
  53,
  'zhangqingyi'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Golden June',
  4,
  45.0,
  0.2,
  'New',
  1,
  './img/painting/thumb/02.jpg',
  'Strong local flavor and unique artistic style',
  1,
  39,
  0,
  53,
  'zhangqingyi'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'The Loess Plateau',
  5,
  228.0,
  0.1,
  'New',
  1,
  './img/painting/thumb/03.jpg',
  'Strong local flavor and unique artistic style',
  1,
  38,
  0,
  53,
  'zhangqingyi'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Harvest Field',
  5,
  126.0,
  0.1,
  'New',
  1,
  './img/painting/thumb/04.jpg',
  'Strong local flavor and unique artistic style',
  1,
  38,
  0,
  53,
  'zhangqingyi'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Sunrise',
  5,
  148.0,
  0.1,
  'New',
  1,
  './img/painting/thumb/05.jpg',
  'Strong local flavor and unique artistic style',
  1,
  38,
  0,
  53,
  'zhangqingyi'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Auspicious Puppy',
  4,
  82.0,
  0.1,
  'New',
  1,
  './img/claysculpture/thumb/01.jpg',
  'This is a traditional folk art. Its form is exaggerated and its color is bright.',
  2,
  22,
  13,
  25,
  'zhangshimin'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Hua Mu Lan',
  4,
  62.0,
  0.2,
  'New',
  1,
  './img/claysculpture/thumb/02.jpg',
  'This is a traditional folk art. Its form is exaggerated and its color is bright.',
  2,
  13,
  6,
  20,
  'zhangshimin'
);


INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Tiger Sitting',
  5,
  1200.0,
  0.1,
  'New',
  1,
  './img/claysculpture/thumb/03.jpg',
  'This is a traditional folk art. Its form is exaggerated and its color is bright.',
  2,
  45,
  6,
  44,
  'zhangshimin'
);

INSERT INTO tgoods (pname, rate, price, sale, label, availability, thumb, detail, type, width, height, length, author)
VALUES (
  'Loong',
  5,
  1200.0,
  0.1,
  'New',
  1,
  './img/claysculpture/thumb/04.jpg',
  'This is a traditional folk art. Its form is exaggerated and its color is bright.',
  2,
  22,
  10,
  21,
  'zhangshimin'
);
